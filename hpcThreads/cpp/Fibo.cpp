#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

namespace Fibo {

  // calcule le Nieme terme de la suite de "Fibonacci modulo 42"
  // precondition : N >= 0
  int FibonacciMod42(int N) {
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++) {
      int tmp = f_curr;
      f_curr = (f_curr + f_prec) % 42;
      f_prec = tmp;
    }
    return f_curr;
  }

  //////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
  void calculerTout(std::vector<int> &data) {
    // effectue tous les calculs
    for (unsigned i=0; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
    }
  };

  std::vector<int> fiboSequentiel(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calcule les donnees sequentiellement
    calculerTout(data);
    return data;
  }

  //////////////////////////////////////////////////////////////////////
 void calculerBlocs(std::vector<int> &data, int a ,int b) {
    for (unsigned i=a; i<b; i++) {
      data[i] = FibonacciMod42(i);
    }
  };
  void calculerBloc1(std::vector<int> &data) {
    for (unsigned i=0; i<data.size(); i=i+2) {
      data[i] = FibonacciMod42(i);
    }
  };
   void calculerBloc2(std::vector<int> &data) {
    for (unsigned i=1; i<data.size(); i=i+2) {
      data[i] = FibonacciMod42(i);
    }
  };
/*
   void calculerBloc1bis(std::vector<int> &data, int a, int b) {
    for (unsigned i=0; i<data.size()/2; i++) {
      data[i] = FibonacciMod42(i);
    }
  };*/
  std::vector<int> fiboBlocs(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, par bloc
    // TODO
	std::thread thread1(calculerBlocs, std::ref(data), 0,data.size()/2);  
    std::thread thread2(calculerBlocs, std::ref(data),data.size()/2 , data.size());  
    thread1.join();  
    thread2.join();
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  std::vector<int> fiboCyclique2(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, cycliquement
    // TODO
	std::thread thread1(calculerBloc1, std::ref(data));  
    std::thread thread2(calculerBloc2, std::ref(data));  
    thread1.join();  
    thread2.join();
    return data;
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  std::vector<int> fiboCycliqueN(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur N threads, cycliquement
    // TODO
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void fiboCycliqueNFake(int nbData, int nbProc) {
    // calculer sur N threads, cycliquement, en ignorant le résultat
    // TODO
  }

}  // namespace Fibo

